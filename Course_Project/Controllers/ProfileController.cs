﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Course_Project.Data;
using Course_Project.Models;
using Course_Project.Models.ProfileViewModels;
using Course_Project.Services;
using Course_Project.Views.Profile;
using Imgur.API;
using Imgur.API.Authentication.Impl;
using Imgur.API.Endpoints.Impl;
using Imgur.API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace Course_Project.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly UrlEncoder _urlEncoder;
        private readonly IStringLocalizer<ProfileController> _localizer;



        private const string AuthenticatorUriFormat = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6";
        private const string RecoveryCodesKey = nameof(RecoveryCodesKey);

        public ProfileController(
          IConfiguration configuration,
          UserManager<ApplicationUser> userManager,
          SignInManager<ApplicationUser> signInManager,
          IEmailSender emailSender,
          ILogger<ProfileController> logger,
          ApplicationDbContext context,
          UrlEncoder urlEncoder,
          IStringLocalizer<ProfileController> localizer)
        {
            _configuration = configuration;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _urlEncoder = urlEncoder;
            _context = context;
            _localizer = localizer;
        }

        [TempData]
        public string StatusMessage { get; set; }


        [HttpGet]
        public async Task<IActionResult> Index(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var user = await SetUserByIdAsync(id);
            CheckUserForNull(user);
            var model = new IndexViewModel
            {
                UserId = user.Id,
                Username = user.UserName,
                Email = user.Email,
                Firstname = user.FirstName,
                Lastname = user.LastName,
                PhoneNumber = user.PhoneNumber,
                IsEmailConfirmed = user.EmailConfirmed,
                StatusMessage = StatusMessage,
                ProfilePicture = user.ProfilePicture
            };
            SetViewData(!await IsRightsForChangeAsync(user, "Admin"));
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(IndexViewModel model)
        {
            var user = await SetUserByIdAsync(model.UserId);
            if (!ModelState.IsValid || !await IsRightsForChangeAsync(user, "Admin"))
            {
                return View(model);
            }

            CheckUserForNull(user);
            var email = user.Email;
            if (model.Email != email)
            {
                var setEmailResult = await _userManager.SetEmailAsync(user, model.Email);
                if (!setEmailResult.Succeeded)
                {
                    throw new ApplicationException($"Unexpected error occurred setting email for user with ID '{user.Id}'.");
                }
            }
            var phoneNumber = user.PhoneNumber;
            if (model.PhoneNumber != phoneNumber)
            {
                var setPhoneResult = await _userManager.SetPhoneNumberAsync(user, model.PhoneNumber);
                if (!setPhoneResult.Succeeded)
                {
                    throw new ApplicationException($"Unexpected error occurred setting phone number for user with ID '{user.Id}'.");
                }
            }
            if (model.Firstname != user.FirstName)
            {
                user.FirstName = model.Firstname;
            }
            if (model.Lastname != user.LastName)
            {
                user.LastName = model.Lastname;
            }
            _context.Users.Update(user);
            await _context.SaveChangesAsync();

            StatusMessage = _localizer["Your profile has been updated"];
            return RedirectToAction("Index", "Profile");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendVerificationEmail(IndexViewModel model)
        {
            var user = await SetUserByIdAsync(model.UserId);
            if (!ModelState.IsValid || !await IsRightsForChangeAsync(user, "Admin"))
            {
                return RedirectToAction("Index", "Profile", new { id = model.UserId });
            }


            CheckUserForNull(user);
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = Url.Action(
                   "ConfirmEmail",
                   "Account",
                   new { userId = user.Id, code },
                   protocol: HttpContext.Request.Scheme);
            EmailService emailService = new EmailService();
            await emailService.SendEmailAsync(model.Email, _localizer["Confirm your account"],
                $"{_localizer["Confirm the registration by clicking on the link:"]} <a href='{callbackUrl}'>ссылка</a>",
                _configuration["Authentication:Mail:Login"],
                _configuration["Authentication:Mail:Password"]);

            StatusMessage = _localizer["Verification email sent. Please check your email."];

            return RedirectToAction("Index", "Profile", new { id = user.Id });
        }

        [HttpGet]
        public async Task<IActionResult> ChangePassword(string id)
        {
            var user = await SetUserByIdAsync(id);
            if (!await IsRightsForChangeAsync(user, "Admin"))
            {
                return RedirectToAction("AccessDenied", "Account");
            }

            CheckUserForNull(user);
            var hasPassword = await _userManager.HasPasswordAsync(user);
            if (!hasPassword)
            {
                return RedirectToAction(nameof(SetPassword), new { id = user.Id });
            }
            var model = new ChangePasswordViewModel { StatusMessage = StatusMessage, UserId = user.Id };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            var user = await SetUserByIdAsync(model.UserId);
            if (!ModelState.IsValid || !await IsRightsForChangeAsync(user, "Admin"))
            {
                return View(model);
            }

            CheckUserForNull(user);
            var changePasswordResult = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (!changePasswordResult.Succeeded)
            {
                AddErrors(changePasswordResult);
                return View(model);
            }
            _logger.LogInformation("User changed their password successfully.");
            StatusMessage = _localizer["Your password has been changed."];
            return RedirectToAction(nameof(ChangePassword));
        }

        [HttpGet]
        public async Task<IActionResult> SetPassword(string id)
        {
            var user = await SetUserByIdAsync(id);
            if (!await IsRightsForChangeAsync(user, "Admin"))
            {
                return RedirectToAction("AccessDenied", "Account");
            }


            CheckUserForNull(user);

            var hasPassword = await _userManager.HasPasswordAsync(user);

            if (hasPassword)
            {
                return RedirectToAction(nameof(ChangePassword), new { id = user.Id });
            }
            var model = new SetPasswordViewModel { StatusMessage = StatusMessage, UserId = user.Id };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetPassword(SetPasswordViewModel model)
        {
            var user = await SetUserByIdAsync(model.UserId);
            if (!ModelState.IsValid || !await IsRightsForChangeAsync(user, "Admin"))
            {
                return View(model);
            }

            CheckUserForNull(user);
            var addPasswordResult = await _userManager.AddPasswordAsync(user, model.NewPassword);
            if (!addPasswordResult.Succeeded)
            {
                AddErrors(addPasswordResult);
                return View(model);
            }
            StatusMessage = _localizer["Your password has been set."];
            return RedirectToAction(nameof(SetPassword), new { id = user.Id });
        }

        public async Task<IActionResult> MyNews(string id)
        {
            var user = await SetUserByIdAsync(id);
            SetViewData(!await IsRightsForChangeAsync(user, "Admin"));
            if (!await _userManager.IsInRoleAsync(user, "Author") && !await _userManager.IsInRoleAsync(user, "Admin"))
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            return View(_context);
        }

        public async Task UploadImageAsync(IList<IFormFile> files)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(ManageNavPages.UserId);
                CheckUserForNull(user);
                var client = new ImgurClient(
                                _configuration["Authentication:Imgur:ClientId"],
                                _configuration["Authentication:Imgur:ClientSecret"]);
                var endpoint = new ImageEndpoint(client);
                IImage image;
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        using (var fileStream = file.OpenReadStream())
                        using (var ms = new MemoryStream())
                        {
                            fileStream.CopyTo(ms);
                            var fileBytes = ms.ToArray();
                            string s = Convert.ToBase64String(fileBytes);
                            image = await endpoint.UploadImageBinaryAsync(fileBytes);
                        }
                        user.ProfilePicture = image.Link;
                        await _userManager.UpdateAsync(user);
                    }
                }
            }
            catch (ImgurException imgurEx)
            {
                Debug.Write("An error occurred uploading an image to Imgur.");
                Debug.Write(imgurEx.Message);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> SetLanguage(string culture)
        {
            var altitudeString = Request.Headers["Referer"].ToString();
            Response.Cookies.Append(".AspNetCore.Culture", $"c={culture}|uic={culture}");
            var user = await _userManager.GetUserAsync(User);
            if (user != null)
            {
                user.Culture = culture;
                await _userManager.UpdateAsync(user);
            }
            return Redirect(altitudeString);
        }


        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private string FormatKey(string unformattedKey)
        {
            var result = new StringBuilder();
            int currentPosition = 0;
            while (currentPosition + 4 < unformattedKey.Length)
            {
                result.Append(unformattedKey.Substring(currentPosition, 4)).Append(" ");
                currentPosition += 4;
            }
            if (currentPosition < unformattedKey.Length)
            {
                result.Append(unformattedKey.Substring(currentPosition));
            }

            return result.ToString().ToLowerInvariant();
        }

        private async Task<bool> IsRightsForChangeAsync(ApplicationUser user, string role)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            var isAdmin = await _userManager.IsInRoleAsync(currentUser, role);
            return (currentUser == user || isAdmin);
        }
        private void CheckUserForNull(ApplicationUser user)
        {
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with this ID.");
            }
        }

        private async Task<ApplicationUser> SetUserByIdAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            ManageNavPages.UserId = user.Id;
            if (!await _userManager.IsInRoleAsync(user, "Author") && !await _userManager.IsInRoleAsync(user, "Admin"))
            {
                ViewData["DisplayNoneAuthor"] = "display:none";
            }
            return user;
        }
        private void SetViewData(bool rights)
        {
            if (rights)
            {
                ViewData["Disabled"] = "disabled";
                ViewData["DisplayNone"] = "display:none";
                ViewData["Hidden"] = "hidden";
            }
        }
        #endregion

    }
}