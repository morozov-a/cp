﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Course_Project.Data;
using Course_Project.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Course_Project.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationDbContext _context;


        public AdminController(
                RoleManager<IdentityRole> roleManager,
                UserManager<ApplicationUser> userManager,
                SignInManager<ApplicationUser> signInManager,
                ApplicationDbContext context)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        public IActionResult Index() => View(_userManager.Users.ToList());

        public async Task<IActionResult> Edit(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                var userRoles = await _userManager.GetRolesAsync(user);
                var allRoles = _roleManager.Roles.ToList();
                ChangeRoleViewModel model = new ChangeRoleViewModel
                {
                    UserId = user.Id,
                    UserEmail = user.Email,
                    UserName = user.UserName,
                    UserRoles = userRoles,
                    AllRoles = allRoles
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Edit(string id, List<string> roles)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                var userRoles = await _userManager.GetRolesAsync(user);
                var allRoles = _roleManager.Roles.ToList();
                var addedRoles = roles.Except(userRoles);
                var removedRoles = userRoles.Except(roles);
                await _userManager.AddToRolesAsync(user, addedRoles);
                await _userManager.RemoveFromRolesAsync(user, removedRoles);
                return RedirectToAction("Index");
            }
            return NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> BlockUser(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                ChangeRoleViewModel model = new ChangeRoleViewModel
                {
                    UserId = user.Id,
                    UserEmail = user.Email,
                    UserName = user.UserName,
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> BlockUser(string id, int timeOfBlock)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (timeOfBlock == 0)
            {
                return RedirectToAction("BlockUser", "Admin", new { userId = user.Id });
            }
            if (user != null)
            {
                user.LockoutEnabled = true;
                user.LockoutEnd = DateTime.Now.AddMinutes(timeOfBlock);
                await _userManager.UpdateAsync(user);
                return RedirectToAction("Index");
            }
            return NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> UnblockUser(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                user.LockoutEnd = null;
                await _userManager.UpdateAsync(user);
                return RedirectToAction("Index");
            }
            return NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> DeleteUser(string id)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                ChangeRoleViewModel model = new ChangeRoleViewModel
                {
                    UserId = user.Id,
                    UserEmail = user.Email,
                    UserName = user.UserName,
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> ConfirmDeleteUser(ChangeRoleViewModel model)
        {
            var user = _context.Users.SingleOrDefault(m => m.Id == model.UserId);
            if (user != null)
            {
                var posts = _context.Posts.Where(m => m.AuthorId == model.UserId).ToList();
                foreach (var post in posts)
                {
                    _context.Remove(post);
                }
                var comments = _context.Comments.Where(m => m.AuthorId == model.UserId).ToList();
                foreach (var comment in comments)
                {
                    _context.Remove(comment);
                }
                var likes = _context.Likes.Where(m => m.UserId == model.UserId).ToList();
                foreach (var like in likes)
                {
                    _context.Remove(like);
                }
                var raitings = _context.Raitings.Where(m => m.UserId == model.UserId).ToList();
                foreach (var raiting in raitings)
                {
                    _context.Remove(raiting);
                }
                _context.Users.Remove(user);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return NotFound();
        }

    }
}