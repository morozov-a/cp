﻿using Course_Project.Data;
using Course_Project.Models;
using Course_Project.Models.PostViewModels;
using Imgur.API;
using Imgur.API.Authentication.Impl;
using Imgur.API.Endpoints.Impl;
using Imgur.API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Course_Project.Controllers
{

    public class PostsController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public PostsController(
            IConfiguration configuration,
            ApplicationDbContext context,
            UserManager<ApplicationUser> userManager)
        {
            _configuration = configuration;
            _context = context;
            _userManager = userManager;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Posts
                 .Include(a => a.Raitings)
                 .Include(b => b.Comments)
                 .Include(c => c.Author);

            return View(await applicationDbContext.ToListAsync());
        }

        [AllowAnonymous]
        public async Task<IActionResult> Top()
        {
            var applicationDbContext = _context.Posts
                  .Include(a => a.Raitings)
                  .Include(b => b.Comments)
                  .Include(c => c.Author);

            return View(await applicationDbContext.ToListAsync());
        }

        [AllowAnonymous]
        public IActionResult Tag(string tagId)
        {
            ViewData["tagId"] = tagId;
            return View();

        }

        [AllowAnonymous]
        public async Task<IActionResult> DisplaySingleCategory()
        {
            var applicationDbContext = _context.Posts
                  .Include(a => a.Raitings)
                  .Include(b => b.Comments)
                  .Include(c => c.Author);

            return View(await applicationDbContext.ToListAsync());
        }

        [Authorize(Roles = "Admin,Author")]
        public IActionResult Create(string id)
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Author")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind("AuthorId, Title, Abstract,Content,Category,TagString")]
            Post post,
            string id)
        {
            if (ModelState.IsValid)
            {
                SetTags(post);
                post.LastModified = DateTime.UtcNow;
                if (await IsRightsForChangeAsync(id))
                {
                    post.Author = await _userManager.FindByIdAsync(id);
                }
                else
                {
                    post.Author = await _userManager.GetUserAsync(User);
                }
                post.CreatedDate = DateTime.UtcNow;
                _context.Add(post);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(post);
        }

        [AllowAnonymous]
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var post = await _context.Posts
                .Include(a => a.Author)
                .Include(c => c.Comments)
                .Include(a => a.Raitings)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (post == null)
            {
                return NotFound();
            }
            return View(post);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> AddComment(string postId, Post post)
        {
            var user = await _userManager.GetUserAsync(User);
            var commentedPost = await _context.Posts
                .Include(a => a.Author)
                .Include(a => a.Raitings)
                .Include(c => c.Comments)
                .SingleOrDefaultAsync(m => m.Id == postId);
            if (post.Comment.Length > 300)
            {
                return PartialView("_CommentsBody", commentedPost);
            }

            _context.Comments.Add(new Comment()
            {
                Text = post.Comment,
                CreatedDate = DateTime.UtcNow,
                Author = user,
                PostId = commentedPost.Id,
                Post = commentedPost
            });
            _context.SaveChanges();
            return PartialView("_CommentsBody", commentedPost);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> AddLike(string Id)
        {
            var comment = _context.Comments
                .Where(a => a.Id == Id)
                .Include(a => a.Author)
                .Include(a => a.Likes)
                .SingleOrDefault();
            var user = await _userManager.GetUserAsync(User);
            var isLiked = _context.Likes
                .Where(n => n.CommentId == comment.Id && n.UserId == user.Id)
                .SingleOrDefault();
            if (isLiked == null)
            {
                Like newLike = new Like()
                {
                    UserId = user.Id,
                    Comment = comment,
                    CommentId = comment.Id
                };

                _context.Likes.Add(newLike);
            }
            else
            {
                _context.Likes.Remove(isLiked);
            }
            _context.SaveChanges();
            return PartialView("_Like", comment);
        }


        [Authorize(Roles = "Admin,Author")]
        public async Task<IActionResult> UploadImageAsync(IFormFile file)
        {
            IImage image = null;
            try
            {
                var client = new ImgurClient(
                                _configuration["Authentication:Imgur:ClientId"],
                                _configuration["Authentication:Imgur:ClientSecret"]);
                var endpoint = new ImageEndpoint(client);
                if (file.Length > 0)
                {
                    using (var fileStream = file.OpenReadStream())
                    using (var ms = new MemoryStream())
                    {
                        fileStream.CopyTo(ms);
                        var fileBytes = ms.ToArray();
                        string s = Convert.ToBase64String(fileBytes);
                        image = await endpoint.UploadImageBinaryAsync(fileBytes);
                    }
                }
            }
            catch (ImgurException imgurEx)
            {
                Debug.Write("An error occurred uploading an image to Imgur.");
                Debug.Write(imgurEx.Message);
            }
            return Json(new { location = image.Link });
        }

        [Authorize(Roles = "Admin,Author")]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var article = await _context.Posts.SingleOrDefaultAsync(m => m.Id == id);
            var userId = article.AuthorId;
            if (!await IsRightsForChangeAsync(userId))
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            if (article == null)
            {
                return NotFound();
            }

            return View(article);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Author")]
        public async Task<IActionResult> Edit(Post post)
        {
            var parentPost = await _context.Posts.SingleOrDefaultAsync(m => m.Id == post.Id);
            var userId = parentPost.AuthorId;
            if (!await IsRightsForChangeAsync(userId))
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            SetTags(post);
            RemoveOldTagsAsync(post);
            parentPost.Content = post.Content;
            parentPost.Category = post.Category;
            parentPost.Abstract = post.Abstract;
            parentPost.TagString = post.TagString;
            parentPost.Title = post.Title;
            parentPost.LastModified = DateTime.UtcNow;
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin,Author")]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var article = await _context.Posts.SingleOrDefaultAsync(m => m.Id == id);
            var userId = article.AuthorId;
            if (!await IsRightsForChangeAsync(userId))
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            if (article == null)
            {
                return NotFound();
            }
            return View(article);
        }

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin,Author")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var article = await _context.Posts.SingleOrDefaultAsync(m => m.Id == id);
            var userId = article.AuthorId;
            if (!await IsRightsForChangeAsync(userId))
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            _context.Posts.Remove(article);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> SetRaiting(int currentValue, string postId)
        {
            var ratedPost = _context.Posts
                .Include(a => a.Raitings)
                .Include(b => b.Comments)
                .Include(c => c.Author)
                .Where(a => a.Id == postId)
                .SingleOrDefault();

            if (currentValue < 1 || currentValue > 5)
            {
                return PartialView("_Raiting", ratedPost);
            }

            var user = await _userManager.GetUserAsync(User);
            var isRated = _context.Raitings
                .Where(n => n.PostId == ratedPost.Id && n.UserId == user.Id)
                .SingleOrDefault();
            if (isRated == null)
            {
                Raiting newRaiting = new Raiting()
                {
                    UserId = user.Id,
                    Post = ratedPost,
                    PostId = ratedPost.Id,
                    Value = currentValue
                };
                _context.Raitings.Add(newRaiting);
            }
            else
            {
                var oldValue = isRated.Value;
                isRated.Value = currentValue;
            }
            _context.SaveChanges();

            return PartialView("_Raiting", ratedPost);
        }

        private async Task<bool> IsRightsForChangeAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            var currentUser = await _userManager.GetUserAsync(User);
            var isAdmin = await _userManager.IsInRoleAsync(currentUser, "Admin");
            return (currentUser == user || isAdmin);
        }

        private void SetTags(Post post)
        {
            var tags = new List<string>();
            char[] delimeterChars = { ' ', ',', '.', ':', '\t', '\n', '\r' };
            if (post.TagString != null)
            {
                string[] words = post.TagString.Split(delimeterChars);
                foreach (var word in words)
                {
                    
                    if (word.Trim() != "" && word.Length < 25)
                    {
                        Tag tag = _context.Tags.SingleOrDefault(n => n.Name == word.ToLower());
                        if (tag == null && !tags.Contains(word.ToLower()))
                        {
                            tag = new Tag() { Name = word.ToLower() };
                            _context.Tags.Add(tag);
                            var postTag = new PostTag
                            {
                                Post = post,
                                Tag = tag
                            };
                            _context.PostTags.Add(postTag);
                            tags.Add(word.Trim());

                        }
                        var postTags = _context.PostTags
                            .Include(t => t.Post)
                            .Include(m => m.Tag)
                            .Where(n => n.PostId == post.Id)
                            .Select(a => a.Tag == tag);
                        if(postTags == null)
                        {
                            var postTag = new PostTag
                            {
                                Post = post,
                                Tag = tag
                            };
                            _context.PostTags.Add(postTag);
                        }
                    }
                }
            }
        }

        private void RemoveOldTagsAsync(Post post)
        {
            var postTags = _context.PostTags.Include(p => p.Post).Include(t => t.Tag).Where(m => m.Post == post);

            foreach (var postTag in postTags)
            {
                var result = 0;
                string[] words = post.TagString.Split("  ");
                foreach (var word in words)
                {
                    if (word.Trim() != "")
                    {
                        if (word == postTag.Tag.Name)
                        {
                            result++;
                            break;
                        }
                    }
                }
                if (result == 0)
                {
                    _context.PostTags.Remove(postTag);
                }
            }
        }
    }
}