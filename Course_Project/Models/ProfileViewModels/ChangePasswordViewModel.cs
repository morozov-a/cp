﻿using System.ComponentModel.DataAnnotations;

namespace Course_Project.Models.ProfileViewModels
{
    public class ChangePasswordViewModel
    {
        [Required(ErrorMessage = "The Current Password field is required.")]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "The New Password field is required.")]
        [StringLength(12, ErrorMessage = "The password must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string UserId { get; set; }

        public string StatusMessage { get; set; }
    }
}
