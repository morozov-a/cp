﻿using System.ComponentModel.DataAnnotations;

namespace Course_Project.Models.ProfileViewModels
{
    public class IndexViewModel
    {
        public string Username { get; set; }

        public bool IsEmailConfirmed { get; set; }

        [Required(ErrorMessage = "The Email field is required.")]
        [EmailAddress(ErrorMessage = "Not a valid email address")]
        public string Email { get; set; }

        [RegularExpression(@"^[A-Za-zа-яА-Я]+$", ErrorMessage = "Name can contain only letters")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "The name must be between {2} and {1} characters")]
        public string Firstname { get; set; }

        [RegularExpression(@"^[A-Za-zа-яА-Я]+$", ErrorMessage = "Surname can contain only letters")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "The last name must be between {2} and {1} characters")]
        public string Lastname { get; set; }

        [Phone(ErrorMessage = "Not a valid phone number")]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }

        public string StatusMessage { get; set; }
        public string UserId { get; set; }
        public string ProfilePicture { get; set; }
    }
}
