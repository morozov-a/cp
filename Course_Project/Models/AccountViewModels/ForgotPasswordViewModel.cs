﻿using System.ComponentModel.DataAnnotations;

namespace Course_Project.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "The Email field is required.")]
        [EmailAddress(ErrorMessage = "Not a valid email address")]
        public string Email { get; set; }
    }
}
