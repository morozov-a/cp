using System.ComponentModel.DataAnnotations;

namespace Course_Project.Models.AccountViewModels
{
    public class ExternalLoginViewModel
    {
        [Required(ErrorMessage = "The Username field is required.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "The Email field is required.")]
        [EmailAddress(ErrorMessage = "Not a valid email address")]
        public string Email { get; set; }
    }
}
