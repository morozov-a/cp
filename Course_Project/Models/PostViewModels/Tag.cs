﻿using System;
using System.Collections.Generic;

namespace Course_Project.Models.PostViewModels
{
    public class Tag
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();

        public string Name { get; set; }

        public List<PostTag> PostTags { get; set; } = new List<PostTag>();
    }
}
