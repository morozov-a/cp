﻿using System;

namespace Course_Project.Models.PostViewModels
{
    public class PostTag
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();

        public string PostId { get; set; }

        public Post Post { get; set; }

        public string TagId { get; set; }

        public Tag Tag { get; set; }
    }
}
