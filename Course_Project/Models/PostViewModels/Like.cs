﻿using System;

namespace Course_Project.Models.PostViewModels
{
    public class Like
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();

        public Comment Comment { get; set; }

        public string CommentId { get; set; }

        public ApplicationUser User { get; set; }

        public string UserId { get; set; }
    }
}
