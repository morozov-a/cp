﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Course_Project.Models.PostViewModels
{
    public class Raiting
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();

        public Post Post { get; set; }

        public string PostId { get; set; }

        public ApplicationUser User { get; set; }

        public string UserId { get; set; }

        [Range(1, 5)]
        public int Value { get; set; }
    }
}
