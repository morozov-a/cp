﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Course_Project.Models.PostViewModels
{
    public class Comment
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();

        public  ApplicationUser Author { get; set; }

        public string AuthorId { get; set; }

        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;

        public Post Post { get; set; }

        public string PostId { get; set; }

        [Required]
        [StringLength(300)]
        public string Text { get; set; }

        public List<Like> Likes { get; set; } = new List<Like>();
    }
}
