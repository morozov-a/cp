﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Course_Project.Models.PostViewModels
{
    public class Post
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();

        public string Category { get; set; }

        [StringLength(100, ErrorMessage = "Maximum {0} characters")]
        public string TagString { get; set; }

        public List<PostTag> PostTags { get; set; } = new List<PostTag>();

        [Required(ErrorMessage = "The Title field is required.")]
        public string Title { get; set; }

        [Display(Name = "Abstract")]
        public string Abstract { get; set; }

        [Required(ErrorMessage = "The Content field is required.")]
        [Display(Name = "Content")]
        public string Content { get; set; }

        public string Comment { get; set; }

        public List<Comment> Comments { get; set; } = new List<Comment>();

        public ApplicationUser Author { get; set; }

        public string AuthorId { get; set; }

        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }

        public DateTime LastModified { get; set; }

        public List<Raiting> Raitings { get; set; } = new List<Raiting>();
    }
}
