﻿using System.ComponentModel.DataAnnotations;

namespace Course_Project.Models.PostViewModels
{
    public class Category
    {
        [Key]
        public string CategoryName { get; set; }
    }
}
