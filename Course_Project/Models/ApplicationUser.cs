﻿using System.Collections.Generic;
using Course_Project.Models.PostViewModels;
using Microsoft.AspNetCore.Identity;

namespace Course_Project.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Culture { get; set; }

        public string ProfilePicture { get; set; }

        public List<Comment> Comments = new List<Comment>();

        public List<Like> Likes = new List<Like>();

        public List<Post> Posts = new List<Post>();

        public List<Raiting> Raitings = new List<Raiting>();
    }
}
