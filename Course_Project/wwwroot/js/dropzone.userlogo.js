 Dropzone.autoDiscover = false;
    var form = document.getElementById("uploader");
    if (form != null) {
        var myDropzone = new Dropzone(form, {
            url: '/Profile/UploadImageAsync',
            maxFilesize: 5,
            maxFiles: 1,
            uploadMultiple: false,
            parallelUploads: 1,
            acceptedFiles: "image/*",
            autoProcessQueue: true,
            init: function () {
                myDropzone = this;
                myDropzone.processQueue();
                this.on("success", function (file) {
                    this.removeFile(file);
                    $('#logoImgDiv').load(document.URL + ' #logoImg');
                    $('#userLogoElem').load(document.URL + ' #userLogo');
                });
            }
        });
    }