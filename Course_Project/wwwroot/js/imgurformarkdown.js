        tinymce.init({
            selector: 'textarea',
            language: "@CultureInfo.CurrentCulture.Name",
            plugins: 'image code',
            toolbar: 'undo redo | image code',
            images_upload_url: '/Posts/UploadImageAsync'
        });