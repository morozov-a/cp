var onSuccessNewComment = function () {
        $("#newComment").val('');
        $('#comments').load(document.URL + " #commentsContainer");
        hubConnection.invoke("Update");
    };

    var onSuccessLike = function () {
        $('#comments').load(document.URL + " #commentsContainer");
        $('#authorLikes').load(document.URL + " #authorLikesCount");
    };
    let hubUrl = '/Posts/Details';
    const hubConnection = new signalR.HubConnection(hubUrl);
    hubConnection.on("Update", function () {
        $('#comments').load(document.URL + " #commentsContainer");
    });
    hubConnection.start();