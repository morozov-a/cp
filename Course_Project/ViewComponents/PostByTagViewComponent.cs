﻿using Course_Project.Data;
using Course_Project.Models.PostViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Course_Project.ViewComponents
{
    public class PostByTagViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;

        public PostByTagViewComponent(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(int howMany, string tagId)
        {
            var postWithCurrentTag = new List<Post>();
            var postTags = await _context.PostTags.Where(t => t.TagId == tagId).ToListAsync();
            foreach (var postTag in postTags)
            {
                postWithCurrentTag.Add(_context.Posts
                                                .Include(a => a.Raitings)
                                                .Include(b => b.Comments)
                                                .Include(c => c.Author)
                                                .SingleOrDefault(m => m.Id == postTag.PostId));
            }
            return View(postWithCurrentTag);
        }
    }
}