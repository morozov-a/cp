﻿using Course_Project.Data;
using Course_Project.Models.PostViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Course_Project.ViewComponents
{
    public class TopPostsViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;

        public TopPostsViewComponent(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(int howMany)
        {
            var sortedPosts = new Dictionary<Post, double>();
            var posts = await _context.Posts
                 .Include(a => a.Author)
                 .Include(a => a.Raitings)
                 .Include(b => b.Comments)
                 .ToListAsync();

            foreach (var post in posts)
            {
                double raiting = 0.0;
                try
                {
                    raiting = await _context.Raitings
                    .Include(a => a.Post)
                    .Where(p => p.Post.Id == post.Id)
                    .Select(v => v.Value)
                    .AverageAsync();
                }
                catch
                {
                    raiting = 0.0;
                }
                finally
                {
                    sortedPosts.Add(post, raiting);
                    sortedPosts = sortedPosts
                        .OrderByDescending(x => x.Value)
                        .ToDictionary(x => x.Key, x => x.Value);
                }
            }
            return View(sortedPosts);
        }
    }
}
