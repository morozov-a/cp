﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Course_Project.Models;
using Course_Project.Models.PostViewModels;

namespace Course_Project.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Post> Posts { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Default> Source { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<Like> Likes { get; set; }

        public DbSet<Raiting> Raitings { get; set; }

        public DbSet<PostTag> PostTags { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>().Ignore(b => b.PhoneNumberConfirmed).Ignore(c => c.TwoFactorEnabled);
            builder.Entity<Tag>().HasAlternateKey(c => c.Name).HasName("AlternateKey_Name");
            builder.Entity("Course_Project.Models.PostViewModels.Comment", b =>
            {
                b.HasOne("Course_Project.Models.ApplicationUser", "Author")
                    .WithMany()
                    .HasForeignKey("AuthorId")
                    .OnDelete(DeleteBehavior.Cascade);

                b.HasOne("Course_Project.Models.PostViewModels.Post", "Post")
                    .WithMany("Comments")
                    .HasForeignKey("PostId")
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity("Course_Project.Models.PostViewModels.Like", b =>
            {
                b.HasOne("Course_Project.Models.PostViewModels.Comment", "Comment")
                    .WithMany("Likes")
                    .HasForeignKey("CommentId")
                    .OnDelete(DeleteBehavior.Cascade);

                b.HasOne("Course_Project.Models.ApplicationUser", "User")
                    .WithMany()
                    .HasForeignKey("UserId");
            });

            builder.Entity("Course_Project.Models.PostViewModels.Post", b =>
            {
                b.HasOne("Course_Project.Models.ApplicationUser", "Author")
                    .WithMany()
                    .HasForeignKey("AuthorId");
                    
            });

            builder.Entity("Course_Project.Models.PostViewModels.PostTag", b =>
            {
                b.HasOne("Course_Project.Models.PostViewModels.Post", "Post")
                    .WithMany("PostTags")
                    .HasForeignKey("PostId")
                    .OnDelete(DeleteBehavior.Cascade);

                b.HasOne("Course_Project.Models.PostViewModels.Tag", "Tag")
                    .WithMany("PostTags")
                    .HasForeignKey("TagId")
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity("Course_Project.Models.PostViewModels.Raiting", b =>
            {
                b.HasOne("Course_Project.Models.PostViewModels.Post", "Post")
                    .WithMany("Raitings")
                    .HasForeignKey("PostId")
                    .OnDelete(DeleteBehavior.Cascade);

                b.HasOne("Course_Project.Models.ApplicationUser", "User")
                    .WithMany()
                    .HasForeignKey("UserId")
                    .OnDelete(DeleteBehavior.Cascade);
            });

        }
    }
}
