﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Course_Project.Data;
using Course_Project.Models;
using Course_Project.Services;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using Course_Project.Hubs;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Course_Project
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR();
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>(opts => {
                opts.Password.RequiredLength = 6;
                opts.Password.RequireNonAlphanumeric = false;
                opts.Password.RequireLowercase = true;
                opts.Password.RequireUppercase = false;
                opts.Password.RequireDigit = true;
                opts.User.RequireUniqueEmail = true;
            })

            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultTokenProviders();

            services.AddAuthentication().AddTwitter(twitterOptions =>
            {
                twitterOptions.ConsumerKey = Configuration["Authentication:Twitter:ConsumerKey"];
                twitterOptions.ConsumerSecret = Configuration["Authentication:Twitter:ConsumerSecret"];
            })
            .AddFacebook(facebookOptions =>
            {
                facebookOptions.AppId = Configuration["Authentication:Facebook:AppId"];
                facebookOptions.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
            });
            //.AddVkontakte("VKontakte", vk =>
            //    {
            //        vk.ClientId = Configuration["Authentication:VKontakte:AppId"];
            //        vk.ClientSecret = Configuration["Authentication:VKontakte:AppSecret"];
            //        vk.Scope.Add("notify");
            //        vk.Scope.Add("offline");
            //        vk.Scope.Add("photos");
            //        vk.CallbackPath = new PathString("/signin-vkontakte");
            //        vk.AuthorizationEndpoint = "https://oauth.vk.com/authorize";
            //        vk.TokenEndpoint = "https://oauth.vk.com/access_token";
            //        vk.UserInformationEndpoint = "https://api.vk.com/method/users.get.json";
            //        vk.Scope.Add("email");


            //    });

             services.AddAuthentication().AddVK("VKontakte",options =>
             {

                 options.ClientId = Configuration["Authentication:VKontakte:AppId"];
                 options.ClientSecret = Configuration["Authentication:VKontakte:AppSecret"];

                 // Request for permissions https://vk.com/dev/permissions?f=1.%20Access%20Permissions%20for%20User%20Token
                 options.Scope.Add("email");

                 // Add fields https://vk.com/dev/objects/user
                 options.Fields.Add("uid");
                 options.Fields.Add("first_name");
                 options.Fields.Add("last_name");

                 // In this case email will return in OAuthTokenResponse, 
                 // but all scope values will be merged with user response
                 // so we can claim it as field
                 options.ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "uid");
                 options.ClaimActions.MapJsonKey(ClaimTypes.Email, "email");
                 options.ClaimActions.MapJsonKey(ClaimTypes.GivenName, "first_name");
                 options.ClaimActions.MapJsonKey(ClaimTypes.Surname, "last_name");
             });
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddMvc()
                    .AddDataAnnotationsLocalization()
                    .AddViewLocalization();
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("be"),
                    new CultureInfo("ru")
                };

                options.DefaultRequestCulture = new RequestCulture("ru");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseSignalR(routes =>
            {
                routes.MapHub<CommentsHub>("/Posts/Details");
            });

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(locOptions.Value);

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "defaultPosts",
                    template: "{controller=Posts}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "defaultProfile",
                    template: "{controller=Profile}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "defaultAdmin",
                    template: "{controller=Admin}/{action=Index}/{id?}");
            });
        }
    }
}
